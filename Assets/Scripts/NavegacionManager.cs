﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class NavegacionManager : Singleton<NavegacionManager>
{
    private NavMeshAgent agent;
    private Animator animator;
    private List<WalkTarget> walkPoints;
    private int actualPoint = 0;
    public bool front;
    public bool back;
    void Start()
    {
        agent = FindObjectOfType<NavMeshAgent>();
        animator = FindObjectOfType<Animator>();
        walkPoints = GetComponentsInChildren<WalkTarget>(true).ToList();
    }

    public void NextWalkPoint()
    {
        if (actualPoint < walkPoints.Count - 1 && (State().Equals(true)))
        {
            actualPoint++;
            agent.isStopped = false;
            agent.destination = walkPoints[actualPoint].transform.position;
            animator.SetBool("walk", true);
            front = true;
            back = false;
        }
    }
    public void PreviousWalkPoint()
    {
        if (actualPoint >= 1 && (State().Equals(true)))
        {
            actualPoint = actualPoint - 1;
            agent.isStopped = false;
            agent.destination = walkPoints[actualPoint].transform.position;
            animator.SetBool("walk", true);
            front = false;
            back = true;
        }
    }
    public bool State()
    {
        return agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending;
    }
}
