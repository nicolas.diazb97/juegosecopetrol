﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameData", menuName
= "PlayerData")]
public class GameData : ScriptableObject
{
    public GameObject[] tutorials;
    public GameObject[] popUps;
    public GameObject[] toolTips;
    public GameObject[] achivement;
    public GameObject[] buttons;
    public int[] intValues;
    public float[] floatValues;
    public string[] stringValues;
    public AnimationClip[] animationClips;
    public Sprite[] sprites;
}
