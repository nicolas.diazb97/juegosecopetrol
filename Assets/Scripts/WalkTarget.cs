﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkTarget : NavegacionManager
{
    public int popUp;
    private UIManager uIManager;
    private Animator animator;
    public bool Parada;
    public NavegacionManager navManager;
    private void Start()
    {
        uIManager = FindObjectOfType<UIManager>();
        animator = FindObjectOfType<Animator>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && (Parada.Equals(true)))
        {
            uIManager.ActivateButton(popUp);
            animator.SetBool("walk", false);
        }
        if (other.tag.Equals("Player") && (Parada.Equals(false)) && navManager.State().Equals(true))
        {
            Debug.Log("corutine");
            StartCoroutine(ContiniousWalk(0.45f, navManager));
        }
    }

    private IEnumerator ContiniousWalk(float _timeWait, NavegacionManager nav)
    {
        yield return new WaitForSeconds(_timeWait);
        Debug.Log("time");
        if (nav.front == true) { nav.NextWalkPoint(); Debug.Log("front"); }
        if (nav.back == true) { nav.PreviousWalkPoint(); Debug.Log("back"); }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Player") && (Parada.Equals(true)))
        {

            uIManager.DeActivateButton(popUp);
        }
    }
}