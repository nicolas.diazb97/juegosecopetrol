﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [Header("ProgressBar")]
    public Slider[] progress;
    [Header("Tutorials")]
    public GameObject[] tutorials;
    [Header("PopUps")]
    public GameObject[] popUps;
    [Header("TooTips")]
    public GameObject[] toolTips;
    [Header("Achivement")]
    public GameObject[] achivements;
    [Header("Buttons")]
    public GameObject[] buttons;
    [Header("AloneSprites")]
    public Image[] sprites;
    [Header("Texts")]
    public Text[] texts;
    [Header("Animation")]
    public Animation[] animations;
    public GameData data;


    //---------TEXT----------//
    public void ActiveText(int _numberOfText)
    {
        texts[_numberOfText].enabled = true;
    }
    public void DeActiveText(int _numberOfText)
    {
        texts[_numberOfText].enabled = false;
    }

    //---------STRING----------//
    public void UpdateString(int _numberOfString)
    {
        texts[_numberOfString].text = data.stringValues[_numberOfString];
    }

    //---------INT----------//
    public void UpdateInt(int _numberOfInt)
    {
        texts[_numberOfInt].text = data.intValues[_numberOfInt].ToString();
    }

    //---------FLOAT----------//
    public void UpdateFloat(int _numberOfFloat)
    {
        texts[_numberOfFloat].text = data.floatValues[_numberOfFloat].ToString();
    }

    //---------SPRITE----------//
    public void UpdateSprite(int _numberOfSprite)
    {
        sprites[_numberOfSprite].sprite = data.sprites[_numberOfSprite];
    }
    public void ActiveSprite(int _numberOfSprite)
    {
        sprites[_numberOfSprite].enabled = true;
    }
    public void DeActiveSprite(int _numberOfSprite)
    {
        sprites[_numberOfSprite].enabled = false;
    }

    //---------SLIDER----------//
    public void UpdateSlider(int _numberOfSlider)
    {
        progress[_numberOfSlider].value = data.intValues[_numberOfSlider];
    }
    public void ActiveSlider(int _numberOfSlider)
    {
        progress[_numberOfSlider].enabled = true;
    }
    public void DeActiveSlider(int _numberOfSlider)
    {
        progress[_numberOfSlider].enabled = false;
    }

    //---------ANIMATION----------//
    public void UpdateAnimationClip(int _numberOfClip)
    {
        animations[_numberOfClip].clip = data.animationClips[_numberOfClip];
    }
    public void ActiveAnimationClip(int _numberOfClip)
    {
        animations[_numberOfClip].Play();
    }
    public void DeActiveAnimationClip(int _numberOfClip)
    {
        animations[_numberOfClip].Stop();
    }


    //---------POPUPS----------//
    public void UpdatePopUp(int _NumberOfGameObjectArray)
    {
        popUps[_NumberOfGameObjectArray] = data.popUps[_NumberOfGameObjectArray];
    }
    public void ActivatePopUp(int _NumberOfGameObjectArray)
    {
        popUps[_NumberOfGameObjectArray].SetActive(true);
    }
    public void DeActivatePopUp(int _NumberOfGameObjectArray)
    {
        popUps[_NumberOfGameObjectArray].SetActive(false);
    }
    public void InstantiatePopUp(int _NumberOfGameObjectArray)
    {
        Instantiate(data.popUps[_NumberOfGameObjectArray], Vector3.zero, Quaternion.identity);
    }

    //---------TOOLTIPS----------//
    public void UpdateToolTip(int _NumberOfGameObjectArray)
    {
        toolTips[_NumberOfGameObjectArray] = data.toolTips[_NumberOfGameObjectArray];
    }
    public void ActivateToolTip(int _NumberOfGameObjectArray)
    {
        toolTips[_NumberOfGameObjectArray].SetActive(true);
    }
    public void DeActivateToolTip(int _NumberOfGameObjectArray)
    {
        toolTips[_NumberOfGameObjectArray].SetActive(false);
    }
    public void InstantiateToolTip(int _NumberOfGameObjectArray)
    {
        Instantiate(data.toolTips[_NumberOfGameObjectArray], Vector3.zero, Quaternion.identity);
    }

    //---------TUTORIALS----------//
    public void UpdateTutorial(int _NumberOfGameObjectArray)
    {
        tutorials[_NumberOfGameObjectArray] = data.tutorials[_NumberOfGameObjectArray];
    }
    public void ActivateTutorial(int _NumberOfGameObjectArray)
    {
        tutorials[_NumberOfGameObjectArray].SetActive(true);
    }
    public void DeActivateTutorial(int _NumberOfGameObjectArray)
    {
        tutorials[_NumberOfGameObjectArray].SetActive(false);
    }
    public void InstantiateTutorial(int _NumberOfGameObjectArray)
    {
        Instantiate(data.tutorials[_NumberOfGameObjectArray], Vector3.zero, Quaternion.identity);
    }

    //---------ACHIVEMENTS----------//
    public void UpdateAchivement(int _NumberOfGameObjectArray)
    {
        achivements[_NumberOfGameObjectArray] = data.achivement[_NumberOfGameObjectArray];
    }
    public void ActivateAchivement(int _NumberOfGameObjectArray)
    {
        achivements[_NumberOfGameObjectArray].SetActive(true);
    }
    public void DeActivateAchivement(int _NumberOfGameObjectArray)
    {
        achivements[_NumberOfGameObjectArray].SetActive(false);
    }
    public void InstantiateAchivement(int _NumberOfGameObjectArray)
    {
        Instantiate(data.achivement[_NumberOfGameObjectArray], Vector3.zero, Quaternion.identity);
    }
    //---------BUTTONS----------//
    public void UpdateButton(int _NumberOfGameObjectArray)
    {
        buttons[_NumberOfGameObjectArray] = data.buttons[_NumberOfGameObjectArray];
    }
    public void ActivateButton(int _NumberOfGameObjectArray)
    {
        buttons[_NumberOfGameObjectArray].SetActive(true);
    }
    public void DeActivateButton(int _NumberOfGameObjectArray)
    {
        buttons[_NumberOfGameObjectArray].SetActive(false);
    }
    public void InstantiateButton(int _NumberOfGameObjectArray)
    {
        Instantiate(data.buttons[_NumberOfGameObjectArray], Vector3.zero, Quaternion.identity);
    }

    //---------GAMEOBJECTS----------//
    public void DestroyGameObject(string _nameToDestoy)
    {
        Destroy(GameObject.Find(_nameToDestoy));
    }
}