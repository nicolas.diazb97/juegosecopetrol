﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : Singleton<TutorialManager>
{
    private int curretScreen=0;
    [Header("Screens")]

    private List<Screen> screens;
    //This method active the next Screen of the List,and deactivate the previous one,then check than it's isnt finish the list yet.
    public void NextScreen()
    {
        if (curretScreen > screens.Count - 1)
        {
            screens[curretScreen].gameObject.SetActive(false);
            curretScreen = 0;
        }
        curretScreen++;
        if (curretScreen >= 1)
        {
            screens[curretScreen-1].gameObject.SetActive(false);
        }
        if (curretScreen < screens.Count)
        {
            screens[curretScreen].gameObject.SetActive(true);

        }
    }
    // Start is called before the first frame update
    public void Start()
    {
        screens = GetComponentsInChildren<Screen>(true).ToList();
        Debug.Log("lista" + screens);
        screens.ForEach(screen => {
            screen.GetComponentInChildren<Button>().onClick.AddListener(() => NextScreen());
        });
        screens[curretScreen].gameObject.SetActive(true);
    }
}
